import mysql from 'mysql2/promise'

export const sqlConnection = await mysql.createConnection({host:'localhost', user: 'root', database: 'playground', port: 3307, password: 'mysecret'});
