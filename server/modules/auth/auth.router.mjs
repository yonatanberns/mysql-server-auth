import bcrypt from 'bcryptjs'
import express from 'express'
import log from '@ajar/marker'
import raw from '../../middleware/route.async.wrapper.mjs'
// import user_model from '../user/user.model.mjs'
import { sqlConnection } from '../../db/mysql.connection.mjs'
import jwt from 'jsonwebtoken'
import passport from 'passport'
import ms from 'ms'
import cookieParser from 'cookie-parser'


const {CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;


import { 
    verify_token, 
    false_response, 
    tokenize 
} from '../../middleware/auth.middleware.mjs'

const router = express.Router()

router.use(express.json());
router.use(cookieParser());

// Passport middleware for each OAuth provider
const facebookAuth = passport.authenticate('facebook', { session: false })
const githubAuth = passport.authenticate('github', { session: false })

// Triggered on the client
router.get('/facebook', facebookAuth)
router.get('/github', githubAuth)


// Triggered by each OAuth provider once the user has authenticated successfully
router.get('/facebook/callback', facebookAuth, (req, res) => {
    const { givenName, familyName } = req.user.name
    const user = { 
      name: `${givenName} ${familyName}`,
      photo: req.user.photos[0].value
    }
    redirect_tokens(req,res,user)
})


router.get('/github/callback', githubAuth , (req, res) => {
    const user = { 
      name: req.user.username,
      photo: req.user.photos[0].value
    }
    redirect_tokens(req,res,user)
})


function redirect_tokens(req,res,user){
    console.log('Enter redirect tokens!!!!!');
    const access_token = jwt.sign({ id : req.user.id , some:'other value'}, APP_SECRET, {
      expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute
    })
    const refresh_token = jwt.sign({ id : req.user.id , profile:JSON.stringify(user)}, APP_SECRET, {
      expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
    })
    // const hour = 3600000;
    // res.cookie('refresh_token',refresh_token, {
    //   maxAge: 60 * 24 * hour, //60 days
    //   httpOnly: true
    // })
    res.cookie('refresh_token',refresh_token, {
      maxAge: ms('60d'), //60 days
      httpOnly: true
    })
    res.redirect(`${CLIENT_ORIGIN}?token=${access_token}&profile=${encodeURIComponent(JSON.stringify(user))}`)
}

router.get('/get-access-token',async (req,res)=> {
    //get refresh_token from client - req.cookies
    const {refresh_token} = req.cookies;
  
    console.log({refresh_token});
  
    if (!refresh_token) return res.status(403).json({
        status:'Unauthorized',
        payload: 'No refresh_token provided.'
    });
  
    try{
      // verifies secret and checks expiration
      const decoded = await jwt.verify(refresh_token, APP_SECRET)
      console.log({decoded})
  
      const {id, profile} = decoded;
  
      const access_token = jwt.sign({ id , some:'other value'}, APP_SECRET, {
          expiresIn: ACCESS_TOKEN_EXPIRATION //expires in 1 minute
      })
      res.status(200).json({access_token, profile })
    }catch(err){
        console.log('error: ',err)
        return res.status(401).json({
            status:'Unauthorized',
            payload: 'Unauthorized - Failed to verify refresh_token.'
        });
    }   
  })



  export const verifyAuth = async (req, res, next) => {
    try {     
        // check header or url parameters or post parameters for token
        const access_token = req.headers['x-access-token'];
  
        if (!access_token) return res.status(403).json({
            status:'Unauthorized',
            payload: 'No token provided.'
        });
  
        // verifies secret and checks exp
        const decoded = await jwt.verify(access_token, APP_SECRET)
  
        // if everything is good, save to request for use in other routes
        req.user_id = decoded.id;
        next();
  
    } catch (error) {
        return res.status(401).json({
            status:'Unauthorized',
            payload: 'Unauthorized - Failed to authenticate token.'
        });
    }
  }  


// Regular connect with register (no third party)
router.post('/register', raw( async (req, res)=> {
    
    log.obj(req.body,'register, req.body:')
    console.log('in register->', req.body);

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    
    log.info('hashedPassword:',hashedPassword)
    let user_data = {
        ...req.body,
        password : hashedPassword
    }
    
    // create a user
    // const created_user = await user_model.create(user_data)
    const [rows,fields] = await sqlConnection.query(`INSERT INTO users (first_name, last_name, email, phone, password)
    VALUES ('${user_data.first_name}','${user_data.last_name}','${user_data.email}','${user_data.phone}','${user_data.password}');`);

    console.log('insretd-->', rows.insertId);

    // const [created_user,fields2] = await sqlConnection.query(`SELECT * FROM users Where email='${user_data.email}'`);
    // if (!created_user[0]) return res.status(404).json({ status: "No user found." });
    // log.obj(created_user[0],'register, created_user:') 
    
    // create a token
    user_data = {...user_data, id: rows.insertId}
    const token = tokenize(rows.insertId) 
    log.info('token:',token)

    return res.status(200).json({ 
        auth: true, 
        token ,
        user : user_data
    })

}))

router.post('/login', raw( async (req, res)=> {

    //extract from req.body the credentials the user entered
    const {email,password} = req.body;

    //look for the user in db by email

    // const user = await user_model.findOne({ email })
    const [user,fields2] = await sqlConnection.query(`SELECT * FROM users Where email='${email}'`);
    console.log('my user->', user[0]);
    if (!user[0]) return res.status(404).json({ status: "No user exist for login." });
    
    // check if the password is valid
    const password_is_valid = await bcrypt.compare(password, user[0].password)
    if (!password_is_valid) return res.status(401).json({...false_response,message:"wrong email or password"})

    
    // if user is found and password is valid
    // create a fresh new token
    const token = tokenize(user[0].id)

    // return the information including token as JSON
    return res.status(200).json({ 
        auth: true, 
        token 
    })
}))

router.get('/logout', raw( async (req, res)=> {
    // return res.status(200).json(false_response)
    req.logout();
    res.clearCookie('refresh_token');
    res.status(200).json({status:'You are logged out'})
}))

router.get('/me', verify_token, raw( async (req, res)=> {
    // const user = await user_model.findById(req.user_id);
    const [user,fields2] = await sqlConnection.query(`SELECT * FROM users Where id='${req.user_id}'`);
    if (!user[0]) return res.status(404).json({ status: "No user exist for login." });
    // if (!user) return res.status(404).json({message:'No user found.'});
    res.status(200).json(user[0]);
}))


router.get('/protected',verifyAuth,(req,res)=>{
    res.status(200).json({status:'OK',payload:`some sensitive data on user id ${req.user_id}`})
})

export default router;